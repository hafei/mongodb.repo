﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    
    public class FindSpec
    {
        // TODO : test with FindOptions is defined. 

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        public static readonly string Documents = "[{ Name : 'Aliquam Company', Description : 'Very Big company'}, { Name : 'Aliquam Company Find Me', Description : 'Very Big company'}, { Name : 'Aliquam Company 2', Description : 'Very Big company'}]";
                
        public class When_find_documents
        {
            static List<BsonDocument> Entities;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                var filter = Builders<BsonDocument>.Filter.AnyIn("Name", new[] { "Aliquam Company", "Aliquam Company Find Me" });
                Entities = service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, null, null, 0, 100, null).Result;

            };

            It should_return_entity = () => Entities.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Entities.Count.ShouldEqual(2);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_find_documents_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();


                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");

                var docs = service.FindAsync(null, Statics.CollectionNames.Companies, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_find_documents_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");
                var docs = service.FindAsync(ConfigurationManager.AppSettings["Token"], null, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
        
        public class When_find_documents_and_filter_is_not_set_should_return_all_documents
        {
            static List<BsonDocument> Entities;
            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                Entities = service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null).Result;
            });

            It should_return_entity = () => Entities.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Entities.Count.ShouldEqual(3);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
    }
}