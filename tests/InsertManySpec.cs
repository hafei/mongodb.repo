﻿using System;
using System.Collections.Generic;
using System.Configuration;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof(InsertMany))]
    public class InsertManySpec
    {
        // TODO : test when InserManyOptions is defined. With validation

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        public static readonly string Documents = "[{ Name : 'Aliquam Company', Description : 'Very Big company'},{ Name : 'Aliquam Company2', Description : 'Very Big company'}]";

        [Subject(typeof(InsertMany))]
        public class When_insert_more_than_one_document
        {
            static List<BsonDocument> Entities;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var insertOptions = new InsertManyOptions { IsOrdered = true };

                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList(), insertOptions).Wait();
                Entities = service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, _ => true, null, null, 0, 2, null).Result;
            };

            It should_return_entities = () => Entities.ShouldNotBeNull();
            It should_return_2_entities = () => Entities.Count.ShouldEqual(2);
            It should_return_right_entity_name = () => Entities[0]["Name"].ToString().ShouldBeLike("Aliquam Company");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(InsertMany))]
        public class When_insert_new_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                service.InsertManyAsync(null, Statics.CollectionNames.Companies, Documents.AsBsonList()).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(InsertMany))]
        public class When_insert_new_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], null, Documents.AsBsonList()).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(InsertMany))]
        public class When_insert_new_document_and_entity_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("entities are not set\r\nParameter name: entities");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

    }
}