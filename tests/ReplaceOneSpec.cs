﻿using System;
using System.Configuration;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;
using ReplaceOneResult = FormSchema.DataContracts.Core.ReplaceOneResult;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof(ReplaceOne))]
    public class ReplaceOneSpec
    {
        // TODO : test when ReplaceOneOptions is defined. With validation

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        public static readonly string Document = "{ Name : 'Aliquam Company For Update', Description : 'Very Big company'}";
        public static readonly string ReplaceDocument = "{ Name : 'Aliquam Company Replaced', Description : 'Very Big company'}";

        [Subject(typeof(ReplaceOne))]
        public class When_replace_document_and_filter_used_as_FilterDefinition
        {
            static ReplaceOneResult replaceOneResult;
            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var newCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", newCompany["Name"].ToString());

                replaceOneResult = (ReplaceOneResult)service.ReplaceOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, BsonDocument.Parse(ReplaceDocument), null).Result;

            };

            It should_find_one_entity = () => replaceOneResult.MatchedCount.ShouldEqual(1);
            It should_replace_one_entity = () => replaceOneResult.ModifiedCount.ShouldEqual(1);
            It should_return_entityid_as_null_because_upsert_was_not_set = () => replaceOneResult.UpsertedId.ShouldBeNull();

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(ReplaceOne))]
        public class When_replace_document_name_and_filter_used_as_FilterDefinition_and_UpdateOptions_with_upsert_true
        {
            static ReplaceOneResult replaceOneResult;
            static BsonDocument Doc;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var newCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies,
                    BsonDocument.Parse(Document)).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Blah");
                var updateOptions = new UpdateOptions { IsUpsert = true };

                replaceOneResult = (ReplaceOneResult)service.ReplaceOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, BsonDocument.Parse(ReplaceDocument), updateOptions).Result;

                var filterAfterUpdate = Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(replaceOneResult.UpsertedId.ToString()));
                Doc = service.FindOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filterAfterUpdate).Result;

            };

            It should_find_one_entity = () => replaceOneResult.MatchedCount.ShouldEqual(0);
            It should_replace_one_entity = () => replaceOneResult.ModifiedCount.ShouldEqual(0);
            It should_return_entityid = () => replaceOneResult.UpsertedId.ToString().ShouldEqual(Doc["_id"].ToString());
            It should_return_the_same_object_with_name = () => Doc["Name"].ShouldEqual("Aliquam Company Replaced");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(ReplaceOne))]
        public class When_replace_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();


                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");

                var updateResult = service.ReplaceOneAsync(null, Statics.CollectionNames.Companies, filter, BsonDocument.Parse(ReplaceDocument), null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(ReplaceOne))]
        public class When_replace_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var updateResult = service.ReplaceOneAsync(ConfigurationManager.AppSettings["Token"], null, filter, BsonDocument.Parse(ReplaceDocument), null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(ReplaceOne))]
        public class When_replace_document_and_replace_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var updateResult = service.ReplaceOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, null, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("entity is not set\r\nParameter name: entity");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(ReplaceOne))]
        public class When_replace_document_and_filter_is_not_set_should_find_first_doc_and_replace_it
        {

            Establish context = () => Bootstrapper.Initialize();
            static ReplaceOneResult replaceOneResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                NewCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).Result;

                replaceOneResult = (ReplaceOneResult)service.ReplaceOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, BsonDocument.Parse(ReplaceDocument), null).GetAwaiter().GetResult();
            };

            It should_find_first_document = () => replaceOneResult.MatchedCount.ShouldEqual(1);
            It shouldnt_replace_document = () => replaceOneResult.ModifiedCount.ShouldEqual(1);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(ReplaceOne))]
        public class When_replace_document_and_filter_is_not_set_should_find_first_doc_and_replace_it_with_upsert_true
        {

            Establish context = () => Bootstrapper.Initialize();
            static ReplaceOneResult replaceOneResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                NewCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).Result;


                var updateOptions = new UpdateOptions() { IsUpsert = true };

                replaceOneResult = (ReplaceOneResult)service.ReplaceOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, BsonDocument.Parse(ReplaceDocument), updateOptions).GetAwaiter().GetResult();
            };

            It should_find_first_document = () => replaceOneResult.MatchedCount.ShouldEqual(1);
            It should_replace_document = () => replaceOneResult.ModifiedCount.ShouldEqual(1);
            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

    }
}