﻿using System;
using System.Configuration;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;
using DeleteResult = FormSchema.DataContracts.Core.DeleteResult;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof(DeleteMany))]
    public class DeleteManySpec
    {
        // TODO : test when DeleteManyOptions is defined. With validation

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        public static readonly string Documents = "[{ Name : 'Aliquam Company For Delete', Description : 'Very Big company'}, { Name : 'Aliquam Company For Delete2', Description : 'Very Big company'}]";

        [Subject(typeof(DeleteMany))]
        public class When_delete_document_and_filter_used_as_FilterDefinition
        {
            static DeleteResult DeleteResult;
            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var docs = Documents.AsBsonList();

                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);
                var filter = Builders<BsonDocument>.Filter.Eq("Name", docs[0]["Name"].ToString());
                DeleteResult = (DeleteResult)service.DeleteManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter).Result;

            };

            It should_delete_one_entity = () => DeleteResult.DeletedCount.ShouldEqual(1);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(DeleteMany))]
        public class When_delete_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();


                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Delete");

                var updateResult = service.DeleteManyAsync(null, Statics.CollectionNames.Companies, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(DeleteMany))]
        public class When_delete_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Delete");
                var updateResult = service.DeleteManyAsync(ConfigurationManager.AppSettings["Token"], null, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(DeleteMany))]
        public class When_delete_document_and_filter_is_not_set_should_find_and_delete_all_documents
        {

            Establish context = () => Bootstrapper.Initialize();
            static DeleteResult DeleteResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                DeleteResult = (DeleteResult)service.DeleteManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null).GetAwaiter().GetResult();
            };

            It should_delete_all_docs = () => DeleteResult.DeletedCount.ShouldEqual(2);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


    }
}