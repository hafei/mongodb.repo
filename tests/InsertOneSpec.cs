﻿using System.Collections.Generic;
using Machine.Specifications;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{    
    public class InsertOneSpec
    {        
        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
               
        public class When_insert_new_document_using_factory
        {
            static Project Entity;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {                

                var projectsRepo = MongoRepositoryFactory.Create<Project>();

                Entity = projectsRepo.InsertOneAsync(new Project()
                {
                    Name = "Project 1",                    
                    Description = "This is project description 1",                    
                    
                }).Result;                
            };

            It should_return_entity = () => Entity.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Entity.ShouldBeOfType(typeof(Project));
            
            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


        public class When_insert_new_document_using_DI
        {
            static Project Entity;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var projectsRepo = Bootstrapper.Resolve<IMongoRepository<Project>>();

                Entity = projectsRepo.InsertOneAsync(new Project()
                {
                    Name = "Project 1",
                    Description = "This is project description 1"
                }).Result;
            };

            It should_return_entity = () => Entity.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Entity.ShouldBeOfType(typeof(Project));

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_insert_new_document_poor_initialization
        {
            static Project Entity;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var projectsRepo = new MongoRepository<Project>();

                Entity = projectsRepo.InsertOneAsync(new Project()
                {
                    Name = "Project 1",
                    Description = "This is project description 1"
                }).Result;
            };

            It should_return_entity = () => Entity.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Entity.ShouldBeOfType(typeof(Project));

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_insert_new_document_usign_bsondocument_and_collection_name
        {
            static BsonDocument Entity;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var docs = new MongoRepository<BsonDocument>();

                var document = new BsonDocument
                    {
                         {"name","Domantas"}, 
                         {"surname","Jovaisas"} 
                    };

                Entity = docs.WithCollection("DynamicCollection").InsertOne(document);
            };

            It should_return_entity = () => Entity.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Entity["_id"].ShouldBeOfType(typeof(BsonObjectId));

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_inserting_new_documents_and_changing_collection_names
        {
            static List<Project> Projects;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var projectsRepo = Bootstrapper.Resolve<IMongoRepository<Project>>();

                projectsRepo.InsertOne(new Project() { Name = "Project 1", Description = "This is project description 1" });
                projectsRepo.WithCollection("SomeTestProjects").InsertOne(new Project() { Name = "Project 2", Description = "This is project description 2" });
                projectsRepo.InsertOne(new Project() { Name = "Project 3", Description = "This is project description 3" });
                projectsRepo.WithCollection("SomeTestProjects").InsertOne(new Project() { Name = "Project 4", Description = "This is project description 4" });
                projectsRepo.InsertOne(new Project() { Name = "Project 5", Description = "This is project description 5" });


                Projects = projectsRepo.WithCollection("SomeTestProjects").Find(Builders<Project>.Filter.Empty);


            };

            It should_return_entity = () => Projects.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Projects.Count.ShouldEqual(4);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        /*
        public class When_insert_new_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var entity = service.InsertOneAsync(null, Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
        
        public class When_insert_new_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var entity = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], null, BsonDocument.Parse(Document)).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
        
        public class When_insert_new_document_and_entity_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var entity = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("entity is not set\r\nParameter name: entity");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
        */
    }
}