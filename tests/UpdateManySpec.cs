﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;
using ServiceStack;
using UpdateResult = FormSchema.DataContracts.Core.UpdateResult;

namespace CodeMash.MongoDB.Repository.Tests
{
    /// <summary>
    /// Class UpdateManySpec. https://docs.mongodb.org/manual/reference/method/db.collection.updateMany/
    /// </summary>
    [Subject(typeof(UpdateMany))]
    public class UpdateManySpec
    {
        // TODO : test when UpdateManyOptions is defined. With validation

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        public static readonly string Documents = "[{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}, { Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}]";

        [Subject(typeof(UpdateMany))]
        public class When_update_documents_name_and_filter_used_as_FilterDefinition
        {
            static UpdateResult UpdateResult;
            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var com1 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;
                var com2 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update Many3");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company For Update Many4");

                UpdateResult = (UpdateResult)service.UpdateManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, update, null).Result;

            };

            It should_find_two_entities = () => UpdateResult.MatchedCount.ShouldEqual(2);
            It should_update_two_entities = () => UpdateResult.ModifiedCount.ShouldEqual(2);
            It should_return_entityid_as_null_because_upsert_was_not_set = () => UpdateResult.UpsertedId.ShouldBeNull();

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateMany))]
        public class When_update_document_name_and_filter_used_as_FilterDefinition_and_UpdateOptions_with_upsert_true
        {
            static UpdateResult UpdateResult;
            static List<BsonDocument> Docs;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var com1 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;
                var com2 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Blah");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company For Update Many4");
                var updateOptions = new UpdateOptions { IsUpsert = true };


                UpdateResult = (UpdateResult)service.UpdateManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, update, updateOptions).Result;

                var filterAfterUpdate = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update Many4");
                Docs = service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filterAfterUpdate, null, null, 0, 1000, null).Result;

            };

            It shouldnt_find_entity = () => UpdateResult.MatchedCount.ShouldEqual(0);
            It shouldnt_update_entity = () => UpdateResult.ModifiedCount.ShouldEqual(0);
            It should_return_entityid = () => UpdateResult.UpsertedId.ToString().ShouldEqual(Docs[Docs.Count - 1]["_id"].ToString());
            It should_return_the_same_object_with_name = () => Docs.Count.ShouldEqual(1);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateMany))]
        public class When_update_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();


                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");

                var updateResult = service.UpdateManyAsync(null, Statics.CollectionNames.Companies, filter, update, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateMany))]
        public class When_update_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");
                var updateResult = service.UpdateManyAsync(ConfigurationManager.AppSettings["Token"], null, filter, update, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateMany))]
        public class When_update_document_and_update_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var updateResult = service.UpdateManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, null, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("update is not set\r\nParameter name: update");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateMany))]
        public class When_update_document_and_filter_is_not_set_should_find_nothing_update_nothing
        {

            Establish context = () => Bootstrapper.Initialize();

            static UpdateResult UpdateResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                // service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList()).Wait(2000);
                var com1 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;
                var com2 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;


                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");

                UpdateResult = (UpdateResult)service.UpdateManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, update, null).GetAwaiter().GetResult();
            };

            It should_find_all_documents = () => UpdateResult.MatchedCount.ShouldEqual(2);
            It should_update_document_all_documents = () => UpdateResult.ModifiedCount.ShouldEqual(2);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateMany))]
        public class When_update_document_and_filter_is_not_set_should_find_nothing_update_nothing_with_upsert_is_true
        {

            Establish context = () => Bootstrapper.Initialize();
            static UpdateResult UpdateResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                // service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList()).Wait(2000);
                var com1 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;
                var com2 = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse("{ Name : 'Aliquam Company For Update Many3', Description : 'Very Big company'}")).Result;


                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");
                var updateOptions = new UpdateOptions() { IsUpsert = true };

                UpdateResult = (UpdateResult)service.UpdateManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, update, updateOptions).GetAwaiter().GetResult();
            };

            It should_find_all_documents = () => UpdateResult.MatchedCount.ShouldEqual(2);
            It should_update_document_all_documents = () => UpdateResult.ModifiedCount.ShouldEqual(2);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

    }
}