﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    
    public class InitSpec
    {       

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
                
        public class When_initialize_with_different_database
        {
            static List<Project> Projects;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var projectsRepo = MongoRepositoryFactory.Create<Project>("mongodb://localhost:27017/test2", "MyProjects");



                projectsRepo.InsertOne(new Project() { Name = "Project 1", Description = "This is project description 1" });
                projectsRepo.InsertOne(new Project() { Name = "Project 2", Description = "This is project description 2" });
                projectsRepo.InsertOne(new Project() { Name = "Project 3", Description = "This is project description 3" });
                projectsRepo.InsertOne(new Project() { Name = "Project 4", Description = "This is project description 4" });
                projectsRepo.InsertOne(new Project() { Name = "Project 5", Description = "This is project description 5" });


                Projects = projectsRepo.Find(_  => true);
                

            };

            It should_return_entity = () => Projects.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Projects.Count.ShouldEqual(5);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        
    }
}