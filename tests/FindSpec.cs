﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    
    public class FindSpec
    {       

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        public static readonly string Documents = "[{ Name : 'Aliquam Company', Description : 'Very Big company'}, { Name : 'Aliquam Company Find Me', Description : 'Very Big company'}, { Name : 'Aliquam Company 2', Description : 'Very Big company'}]";
                
        public class When_find_documents_count_must_match_and_filter_as_linq_expression
        {
            static List<Project> Projects;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var projectsRepo = Bootstrapper.Resolve<IMongoRepository<Project>>();



                projectsRepo.InsertOne(new Project() { Name = "Project 1", Description = "This is project description 1" });
                projectsRepo.InsertOne(new Project() { Name = "Project 2", Description = "This is project description 2" });
                projectsRepo.InsertOne(new Project() { Name = "Project 3", Description = "This is project description 3" });
                projectsRepo.InsertOne(new Project() { Name = "Project 4", Description = "This is project description 4" });
                projectsRepo.InsertOne(new Project() { Name = "Project 5", Description = "This is project description 5" });


                Projects = projectsRepo.Find(_  => true);
                

            };

            It should_return_entity = () => Projects.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Projects.Count.ShouldEqual(5);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


        public class When_find_documents_count_must_match_and_filter_doesnt_provided
        {
            static List<Project> Projects;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var projectsRepo = Bootstrapper.Resolve<IMongoRepository<Project>>();

                projectsRepo.InsertOne(new Project() { Name = "Project 1", Description = "This is project description 1" });
                projectsRepo.InsertOne(new Project() { Name = "Project 2", Description = "This is project description 2" });
                projectsRepo.InsertOne(new Project() { Name = "Project 3", Description = "This is project description 3" });
                projectsRepo.InsertOne(new Project() { Name = "Project 4", Description = "This is project description 4" });
                projectsRepo.InsertOne(new Project() { Name = "Project 5", Description = "This is project description 5" });


                Projects = projectsRepo.Find(null);


            };

            It should_return_entity = () => Projects.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Projects.Count.ShouldEqual(5);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_find_documents_count_must_match_and_empty_filter_provided_using_mongodb_builder
        {
            static List<Project> Projects;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var projectsRepo = Bootstrapper.Resolve<IMongoRepository<Project>>();

                projectsRepo.InsertOne(new Project() { Name = "Project 1", Description = "This is project description 1" });
                projectsRepo.InsertOne(new Project() { Name = "Project 2", Description = "This is project description 2" });
                projectsRepo.InsertOne(new Project() { Name = "Project 3", Description = "This is project description 3" });
                projectsRepo.InsertOne(new Project() { Name = "Project 4", Description = "This is project description 4" });
                projectsRepo.InsertOne(new Project() { Name = "Project 5", Description = "This is project description 5" });


                Projects = projectsRepo.Find(Builders<Project>.Filter.Empty);


            };

            It should_return_entity = () => Projects.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => Projects.Count.ShouldEqual(5);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
    }
}