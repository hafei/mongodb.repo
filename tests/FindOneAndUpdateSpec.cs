﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof(FindOneAndUpdate))]
    public class FindOneAndUpdateSpec
    {
        // TODO : test with FindOptions is defined. 

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        public static readonly string Documents = "[{ Name : 'Aliquam Company', Description : 'Very Big company'}, { Name : 'Aliquam Company Find Me', Description : 'Very Big company'}, { Name : 'Aliquam Company 2', Description : 'Very Big company'}]";

        [Subject(typeof(FindOneAndUpdate))]
        public class When_find_and_update_document
        {
            static BsonDocument UpdatedEntity;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                var update = Builders<BsonDocument>.Update.Set("Name", "New Name");
                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");

                var options = new FindOneAndUpdateOptions<BsonDocument> { ReturnDocument = ReturnDocument.After };

                UpdatedEntity = service.FindOneAndUpdateAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, update, options).Result;

            };

            It should_return_entity = () => UpdatedEntity.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => UpdatedEntity.ShouldBeOfType(typeof(BsonDocument));
            It should_return_entity_with_id = () => UpdatedEntity["_id"].ShouldNotBeNull();
            It should_be_id_of_type_ObjectId = () => UpdatedEntity["_id"].ShouldBeOfType(typeof(BsonObjectId));
            It should_return_the_same_object_with_name = () => UpdatedEntity["Name"].ShouldEqual("New Name");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");
                var doc = service.FindOneAndUpdateAsync(null, Statics.CollectionNames.Companies, filter, new BsonDocument()).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");
                var doc = service.FindOneAndUpdateAsync(ConfigurationManager.AppSettings["Token"], null, filter, new BsonDocument()).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        /// <exclude />
        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_filter_is_not_set_should_update_first_doc
        {
            static List<BsonDocument> Entities;
            static BsonDocument UpdatedEntity;
            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                var update = Builders<BsonDocument>.Update.Set("Name", "New Name");

                var options = new FindOneAndUpdateOptions<BsonDocument> { ReturnDocument = ReturnDocument.After };

                UpdatedEntity = service.FindOneAndUpdateAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, update, options).Result;

                Entities =
                    service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null)
                        .Result;

            });

            It should_return_first_entity = () => Entities.ShouldNotBeNull();
            It should_return_two_entities_with_the_same_name = () => Entities.Count.ShouldEqual(3);
            It should_return_the_same_object_with_name = () => Entities[0]["Name"].ShouldEqual("New Name");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
    }
}