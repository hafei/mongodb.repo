﻿using System.Collections.Generic;

namespace CodeMash.MongoDB.Repository.Tests
{    
    public class ResourceCategory
	{
        public ResourceCategory()
        {
            Keys = new List<ResourceKey>();
        }
        
        public string Key { get; set; }        
        public string Name { get; set; }        
        public List<ResourceKey> Keys { get; set; }        
	}
}
