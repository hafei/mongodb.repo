﻿using System.Collections.Generic;

namespace CodeMash.MongoDB.Repository.Tests
{
    
    public class ResourceLanguage : Entity
    {
        public ResourceLanguage()
        {
            DefaultValues = new List<ResourceValue>();
        }
        
        public string CultureCode { get; set; }        
        public string Name { get; set; }        
        public string NativeName { get; set; }        
        public List<ResourceValue> DefaultValues { get; set; }
	}
}
