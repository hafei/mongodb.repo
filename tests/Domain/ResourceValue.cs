﻿using MongoDB.Bson;

namespace CodeMash.MongoDB.Repository.Tests
{
    public class ResourceValue 
    {       
        public string Value { get; set; }
        public ObjectId ResourceLanguageId { get; set; }
	}
}
