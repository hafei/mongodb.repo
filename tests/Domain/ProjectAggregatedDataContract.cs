namespace CodeMash.MongoDB.Repository.Tests
{
    public class ProjectAggregatedDataContract : Entity
    {        
        public string Name { get; set; }        
        public int CategoriesDistinct { get; set; }
    }
}