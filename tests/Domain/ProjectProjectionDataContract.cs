﻿using System.Collections.Generic;

namespace CodeMash.MongoDB.Repository.Tests.DataContracts
{
    public class ProjectProjectionDataContract
    {
        public List<ResourceCategory> Categories { get; set; }
    }
}
