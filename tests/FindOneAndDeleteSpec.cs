﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof(FindOneAndDelete))]
    public class FindOneAndDeleteSpec
    {
        // TODO : test with FindOptions is defined. 

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        public static readonly string Documents = "[{ Name : 'Aliquam Company', Description : 'Very Big company'}, { Name : 'Aliquam Company Find Me', Description : 'Very Big company'}, { Name : 'Aliquam Company 2', Description : 'Very Big company'}]";

        [Subject(typeof(FindOneAndDelete))]
        public class When_find_document
        {
            static List<BsonDocument> Entities;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                var filter = Builders<BsonDocument>.Filter.Eq("Name", docs[1]["Name"].ToString());
                var findOneAndDeleteResult = service.FindOneAndDeleteAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter).Result;

                Entities =
                    service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null)
                        .Result;


            };

            It should_return_entity = () => Entities.ShouldNotBeNull();
            It should_return_the_same_object_with_name = () => Entities.Count.ShouldEqual(2);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");

                var doc = service.FindOneAndDeleteAsync(null, Statics.CollectionNames.Companies, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");
                var doc = service.FindOneAndDeleteAsync(ConfigurationManager.AppSettings["Token"], null, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        /// <exclude />
        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_filter_is_not_set_should_delete_first_doc_in_collection
        {
            static List<BsonDocument> Entities;
            static BsonDocument NewEntity;
            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                var deleteResult = service.FindOneAndDeleteAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null).Result;

                Entities = service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null).Result;

            });

            It should_return_first_entity = () => Entities.ShouldNotBeNull();
            It should_return_two_entities_with_the_same_name = () => Entities.Count.ShouldEqual(2);
            It should_return_entity_with_right_name = () => Entities[0]["Name"].ShouldEqual("Aliquam Company Find Me");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
    }
}