﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using CodeMash.MongoDB.Repository.Tests.DataContracts;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof (Aggregate))]
    public class AggregateSpec
    {

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        [Subject(typeof(ResourceCategory))]
        public class When_ask_get_aggregated_version
        {
            static List<BsonDocument> Entities;

            Establish context = () => Bootstrapper.Initialize();
            public static readonly string Documents = "[{ Name : 'TestProject', Categories : [ 1,2,3,4,5,6]},{ Name : 'TestProject2', Categories : [ 1,2,3]}]";

            // db.Companies.aggregate([
            //    { "$match" : { "Name" : "TestProject" }}, 
            //    { $unwind : "$Categories"}, 
            //    { $group : { _id : { Name : "$Name"},   "CategoriesCount" : { "$sum" : 1 } } },
            //    { $project : { Name : "$_id.Name", "CategoriesCount" : 1, _id : 0}}
            // ]);

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var insertOptions = new InsertManyOptions { IsOrdered = true };
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList(), insertOptions).Wait();

                var match = new BsonDocument
                {
                    {
                        "$match",
                        new BsonDocument
                        {
                            {"Name", "TestProject"}
                        }
                    }
                };


                var unwind = new BsonDocument
                {
                    {
                        "$unwind",
                        "$Categories"
                    }
                };

                var group = new BsonDocument
                {
                    {
                        "$group",
                        new BsonDocument
                        {
                            { "_id", new BsonDocument {{ "Name","$Name" }}},
                            { "CategoriesCount", new BsonDocument
                                {
                                    {
                                        "$sum", 1
                                    }
                                }
                            }
                        }
                    }
                };

                var project = new BsonDocument
                {
                    {
                        "$project",
                        new BsonDocument
                        {
                            { "Name", "$_id.Name"},
                            { "CategoriesCount", 1 },
                            { "_id", 0 }
                        }
                    }
                };

                var pipeline = new[] { match, unwind, group, project };

                Entities = service.AggregateAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, pipeline, null).Result;
            };

            It should_return_aggregated_data = () => Entities.ShouldNotBeNull();
            It should_return_aggregated_data_only_two_record = () => Entities.Count.ShouldEqual(1);
            It should_return_aggregated_data_first_record_with6_categories = () => Entities.First()["CategoriesCount"].ShouldEqual(6);


            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
        

        [Subject(typeof(UpdateOne))]
        public class When_aggregate_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var doc = service.AggregateAsync(null, Statics.CollectionNames.Companies, new[] { new BsonDocument() }, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_aggregate_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var doc = service.AggregateAsync(ConfigurationManager.AppSettings["Token"], null, new[] { new BsonDocument() }, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_aggregate_document_and_aggregation_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var doc = service.AggregateAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("aggregation is not set\r\nParameter name: aggregation");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
    }
}