﻿using System;
using System.Configuration;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    public class UpdateOneSpec
    {
        // TODO : test when UpdateOneOptions is defined. With validation

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        public static readonly string Document = "{ Name : 'Aliquam Company For Update', Description : 'Very Big company'}";

        public class When_update_document_name_and_filter_used_as_FilterDefinition
        {
            static UpdateResult UpdateResult;
            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var newCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies,
                    BsonDocument.Parse(Document)).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", newCompany["Name"].ToString());
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");

                UpdateResult = (UpdateResult)service.UpdateOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, update, null).Result;

            };

            It should_find_one_entity = () => UpdateResult.MatchedCount.ShouldEqual(1);
            It should_update_one_entity = () => UpdateResult.ModifiedCount.ShouldEqual(1);
            It should_return_entityid_as_null_because_upsert_was_not_set = () => UpdateResult.UpsertedId.ShouldBeNull();

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
                
        public class When_update_document_name_and_filter_used_as_FilterDefinition_and_UpdateOptions_with_upsert_true
        {
            static UpdateResult UpdateResult;
            static BsonDocument Doc;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var newCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies,
                    BsonDocument.Parse(Document)).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Blah");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");
                var updateOptions = new UpdateOptions() { IsUpsert = true };

                UpdateResult = (UpdateResult)service.UpdateOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, update, updateOptions).Result;


                var filterAfterUpdate = Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(UpdateResult.UpsertedId.ToString()));
                Doc = service.FindOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filterAfterUpdate).Result;

            };

            It should_find_one_entity = () => UpdateResult.MatchedCount.ShouldEqual(0);
            It should_update_one_entity = () => UpdateResult.ModifiedCount.ShouldEqual(0);
            It should_return_entityid = () => UpdateResult.UpsertedId.ToString().ShouldEqual(Doc["_id"].ToString());
            It should_return_the_same_object_with_name = () => Doc["Name"].ShouldEqual("Aliquam Company");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_update_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();


                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");

                var updateResult = service.UpdateOneAsync(null, Statics.CollectionNames.Companies, filter, update, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_update_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");
                var updateResult = service.UpdateOneAsync(ConfigurationManager.AppSettings["Token"], null, filter, update, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
                
        public class When_update_document_and_update_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Update");
                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");
                var updateResult = service.UpdateOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter, null, null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("update is not set\r\nParameter name: update");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        public class When_update_document_and_filter_is_not_set_should_find_and_modify_unless_upsert_is_used
        {

            Establish context = () => Bootstrapper.Initialize();
            static UpdateResult UpdateResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                NewCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).Result;

                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");

                UpdateResult = (UpdateResult)service.UpdateOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, update, null).GetAwaiter().GetResult();
            };

            It should_find_first_document = () => UpdateResult.MatchedCount.ShouldEqual(1);
            It shouldnt_update_document = () => UpdateResult.ModifiedCount.ShouldEqual(1);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_update_document_and_filter_is_not_set_should_find_and_modify_because_upsert_is_used
        {

            Establish context = () => Bootstrapper.Initialize();
            static UpdateResult UpdateResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                NewCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).Result;

                var update = Builders<BsonDocument>.Update.Set("Name", "Aliquam Company");
                var updateOptions = new UpdateOptions() { IsUpsert = true };

                UpdateResult = (UpdateResult)service.UpdateOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, update, updateOptions).GetAwaiter().GetResult();
            };

            It should_find_first_document = () => UpdateResult.MatchedCount.ShouldEqual(1);
            It should_update_document = () => UpdateResult.ModifiedCount.ShouldEqual(1);
            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

    }
}