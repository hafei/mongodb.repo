namespace CodeMash.MongoDB.Repository.Tests
{
    public static class Statics
    {

        public static class CollectionNames
        {
            public static readonly string Companies = "Companies";
            public static readonly string ClientConnections = "ClientConnections";



            public static readonly string Users = "Users";
            public static readonly string Languages = "Languages";
            public static readonly string Projects = "Projects";
            public static readonly string ResourceLanguages = "ResourceLanguages";
            public static readonly string ResourceKeys = "ResourceKeys";
            public static readonly string ResourceValues = "ResourceKeys";

        }
    }
}