﻿using MongoDB.Bson;
using System.Configuration;
using MongoDB.Driver;
using Microsoft.Practices.Unity;
using System;

namespace CodeMash.MongoDB.Repository.Tests
{
    public static class Bootstrapper
    {
        public static IUnityContainer Container;

        public static void Initialize()
        {
            Container = BuildUnityContainer();
        }

        public static string ApiUrl => ConfigurationManager.AppSettings["FormSchemaApiAddress"];

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            container.RegisterInstance("MyConnectionString", new MongoUrl("mongodb://localhost"), new ContainerControlledLifetimeManager());
            container.RegisterInstance("Collection", "CollectionName", new ContainerControlledLifetimeManager());

            container.RegisterType(typeof(IMongoRepository<>), typeof(MongoRepository<>), new InjectionConstructor(new ResolvedParameter<MongoUrl>("MyConnectionString")));//, new ResolvedParameter<string>("Collection")));
            return container;
        }

        /// <summary>
        /// Cleans up project scope.
        /// </summary>
        public static void CleanUpProjectScope()
        {
            var projectsRepo = Resolve<IMongoRepository<Project>>();
            projectsRepo.DeleteMany(_ => true);
            projectsRepo.WithCollection("SomeTestProjects").DeleteMany(_ => true);
            projectsRepo.WithCollection("DynamicCollection").DeleteMany(_ => true);


            var projectsRepo2  = MongoRepositoryFactory.Create<Project>("mongodb://localhost:27017/test2", "MyProjects");
            projectsRepo2.DeleteMany(_ => true);

            Container.Dispose();
        }

        public static T Resolve<T>()
        {
            
            T resolved;

                //if (Container.IsRegistered<T>()) 
                try
                {
                    resolved = Container.Resolve<T>();
                }
                catch (Exception e)
                {
                    resolved = default(T);
                }
                return resolved;
        }

    }
}   