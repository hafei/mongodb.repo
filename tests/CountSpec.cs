﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof (Count))]
    public class CountSpec
    {

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        [Subject(typeof(ResourceCategory))]
        public class When_ask_get_count_wiht_filter
        {
            static List<BsonDocument> Entities;
            static long Count;

            Establish context = () => Bootstrapper.Initialize();
            public static readonly string Documents = "[{ Name : 'TestProject'},{ Name : 'TestProject'},{ Name : 'TestProject2'},{ Name : 'TestProject2'}]";
            
            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var insertOptions = new InsertManyOptions { IsOrdered = true };
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList(), insertOptions).Wait();
                
                Count = service.CountAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, "{ Name : 'TestProject'}", null).Result;
            };

            It should_return_count_2 = () => Count.ShouldEqual(2);
            

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
        

        [Subject(typeof(UpdateOne))]
        public class When_count_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var doc = service.CountAsync(null, Statics.CollectionNames.Companies, new BsonDocument(), null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_count_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var doc = service.CountAsync(ConfigurationManager.AppSettings["Token"], null, new BsonDocument(), null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_count_document_and_filter_is_not_set
        {
            static long Count;
            Establish context = () => Bootstrapper.Initialize();
            public static readonly string Documents = "[{ Name : 'TestProject'},{ Name : 'TestProject'},{ Name : 'TestProject2'},{ Name : 'TestProject2'}]";

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var insertOptions = new InsertManyOptions { IsOrdered = true };
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList(), insertOptions).Wait();

                Count = service.CountAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, null).Result;
            };

            It should_return_count_4 = () => Count.ShouldEqual(4);


            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
    }
}