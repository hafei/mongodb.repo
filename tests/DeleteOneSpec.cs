﻿using System;
using System.Configuration;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;
using DeleteResult = FormSchema.DataContracts.Core.DeleteResult;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof(DeleteOne))]
    public class DeleteOneSpec
    {
        // TODO : test when DeleteOneOptions is defined. With validation

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        public static readonly string Document = "{ Name : 'Aliquam Company For Delete', Description : 'Very Big company'}";

        [Subject(typeof(DeleteOne))]
        public class When_delete_document_and_filter_used_as_FilterDefinition
        {
            static DeleteResult DeleteResult;
            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var newCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", newCompany["Name"].ToString());

                DeleteResult = (DeleteResult)service.DeleteOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter).Result;

            };

            It should_delete_one_entity = () => DeleteResult.DeletedCount.ShouldEqual(1);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(DeleteOne))]
        public class When_delete_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();


                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Delete");

                var updateResult = service.DeleteOneAsync(null, Statics.CollectionNames.Companies, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(DeleteOne))]
        public class When_delete_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company For Delete");
                var updateResult = service.DeleteOneAsync(ConfigurationManager.AppSettings["Token"], null, filter).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(DeleteOne))]
        public class When_delete_document_and_filter_is_not_set_should_find_first_doc_and_delete_it
        {

            Establish context = () => Bootstrapper.Initialize();
            static DeleteResult DeleteResult;
            static BsonDocument NewCompany;

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                NewCompany = service.InsertOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, BsonDocument.Parse(Document)).Result;

                DeleteResult = (DeleteResult)service.DeleteOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null).GetAwaiter().GetResult();
            };

            It should_find_first_document = () => DeleteResult.DeletedCount.ShouldEqual(1);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


    }
}