﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;
using ServiceStack.OrmLite;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof (Distinct))]
    public class DistinctSpec
    {

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();

        [Subject(typeof(ResourceCategory))]
        public class When_ask_get_Distinct_wiht_filter
        {
            static List<string> Entities;
            
            Establish context = () => Bootstrapper.Initialize();
            public static readonly string Documents = "[{ Name : 'TestProject'},{ Name : 'TestProject'},{ Name : 'TestProject2'},{ Name : 'TestProject2'}]";
            
            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var insertOptions = new InsertManyOptions { IsOrdered = true };
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList(), insertOptions).Wait();

                var field = new StringFieldDefinition<BsonDocument, BsonDocument>("Name");

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "TestProject");
                
                Entities = service.DistinctAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, "Name", filter, null).Result;
            };

            It should_return_items = () => Entities.ShouldNotBeNull();
            It should_return_1_items_in_list = () => Entities.Count.ShouldEqual(1);
            

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(ResourceCategory))]
        public class When_ask_get_Distinct_wihtout_filter
        {
            static List<string> Entities;

            Establish context = () => Bootstrapper.Initialize();
            public static readonly string Documents = "[{ Name : 'TestProject'},{ Name : 'TestProject'},{ Name : 'TestProject2'},{ Name : 'TestProject2'}]";

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var insertOptions = new InsertManyOptions { IsOrdered = true };
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, Documents.AsBsonList(), insertOptions).Wait();

                var field = new StringFieldDefinition<BsonDocument, BsonDocument>("Name");

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "TestProject");
                Entities = service.DistinctAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, "Name", filter, null).Result;
            };

            It should_return_items = () => Entities.ShouldNotBeNull();
            It should_return_2_items_in_list = () => Entities.Count.ShouldEqual(1);


            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


        [Subject(typeof(UpdateOne))]
        public class When_Distinct_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var doc = service.DistinctAsync(null, Statics.CollectionNames.Companies, "fieldDefinition", new BsonDocument(), null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_Distinct_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var doc = service.DistinctAsync(ConfigurationManager.AppSettings["Token"], null, "fieldDefinition", new BsonDocument(), null).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
        
    }
}