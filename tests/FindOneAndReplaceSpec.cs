﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FormSchema.DataContracts.Core;
using FormSchema.Service;
using Machine.Specifications;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CodeMash.MongoDB.Repository.Tests
{
    [Subject(typeof(FindOneAndReplace))]
    public class FindOneAndReplaceSpec
    {
        // TODO : test with FindOptions is defined. 

        Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        public static readonly string Documents = "[{ Name : 'Aliquam Company', Description : 'Very Big company'}, { Name : 'Aliquam Company Find Me', Description : 'Very Big company'}, { Name : 'Aliquam Company 2', Description : 'Very Big company'}]";

        [Subject(typeof(FindOneAndReplace))]
        public class When_find_document
        {
            static BsonDocument Entity;
            static BsonDocument NewEntity;

            Establish context = () => Bootstrapper.Initialize();

            Because of = () =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();
                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                var filter = Builders<BsonDocument>.Filter.Eq("Name", docs[1]["Name"].ToString());
                Entity = service.FindOneAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter).Result;

                var filterbyId = Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(Entity["_id"].ToString()));

                Entity["Name"] = "You replaced me.";

                var options = new FindOneAndReplaceOptions<BsonDocument> { ReturnDocument = ReturnDocument.After };

                NewEntity = service.FindOneAndReplaceAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filterbyId, Entity, options).Result;

            };

            It should_return_entity = () => NewEntity.ShouldNotBeNull();
            It should_return_entity_as_bsondocument = () => NewEntity.ShouldBeOfType(typeof(BsonDocument));
            It should_return_entity_with_id = () => NewEntity["_id"].ShouldNotBeNull();
            It should_be_id_of_type_ObjectId = () => NewEntity["_id"].ShouldBeOfType(typeof(BsonObjectId));
            It should_return_the_same_object_with_name = () => NewEntity["Name"].ShouldEqual("You replaced me.");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }


        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_token_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();


                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");

                var doc = service.FindOneAndReplaceAsync(null, Statics.CollectionNames.Companies, filter, new BsonDocument()).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("token is not set\r\nParameter name: token");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_collectionName_is_not_set
        {

            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company");
                var doc = service.FindOneAndReplaceAsync(ConfigurationManager.AppSettings["Token"], null, filter, new BsonDocument()).GetAwaiter().GetResult();
            });

            It should_throw_exception = () => exception.ShouldBeOfType<ArgumentNullException>();
            It should_throw_right_message = () => exception.Message.ShouldEqual("collectionName is not set\r\nParameter name: collectionName");

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }

        /// <exclude />
        [Subject(typeof(UpdateOne))]
        public class When_find_document_and_filter_is_not_set_should_replace_first_doc
        {
            static List<BsonDocument> Entities;
            static BsonDocument NewEntity;
            Establish context = () => Bootstrapper.Initialize();

            static Exception exception;

            Because of = () => exception = Catch.Exception(() =>
            {
                var service = Bootstrapper.Container.Resolve<IFsDataContext>();

                var docs = Documents.AsBsonList();
                service.InsertManyAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, docs).Wait(2000);

                NewEntity = service.FindOneAndReplaceAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, null, BsonDocument.Parse("{ Name : 'Aliquam Company Find Me', Description : 'Very Big company'}"), null).Result;

                var filter = Builders<BsonDocument>.Filter.Eq("Name", "Aliquam Company Find Me");
                Entities = service.FindAsync(ConfigurationManager.AppSettings["Token"], Statics.CollectionNames.Companies, filter).Result;

            });

            It should_return_first_entity = () => Entities.ShouldNotBeNull();
            It should_return_two_entities_with_the_same_name = () => Entities.Count.ShouldEqual(2);

            Cleanup after = () => Bootstrapper.CleanUpProjectScope();
        }
    }
}