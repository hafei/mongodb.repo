CodeMash.MongoDb.Repository is an easy to use library to use MongoDB with .NET.
It implements a Repository pattern on top of Official MongoDB C# driver. Meaning, CodeMash.MongoDb.Repository serves as an intermidiate layer between Mongo DB and business layer of your .NET aplication.

1. [Documents and collections](https://bitbucket.org/codemash-io/mongodb.repo/wiki/Documents%20and%20collections)
2. [Getting started](https://bitbucket.org/codemash-io/mongodb.repo/wiki/Getting%20started)
3. [Connecting to database](https://bitbucket.org/codemash-io/mongodb.repo/wiki/Connecting%20to%20database)
4. CRUD operations  
4.1. [Create](https://bitbucket.org/codemash-io/mongodb.repo/wiki/Create)  
4.2. [Read](https://bitbucket.org/codemash-io/mongodb.repo/wiki/Read)  
4.3. [Update](https://bitbucket.org/codemash-io/mongodb.repo/wiki/Update)  
4.4. [Delete](https://bitbucket.org/codemash-io/mongodb.repo/wiki/Delete)  