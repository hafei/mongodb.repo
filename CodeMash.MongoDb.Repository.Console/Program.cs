﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeMash.MongoDB.Repository;
using MongoDB.Driver;

namespace CodeMash.MongoDb.Repository
{

    [CollectionName("Connections")]
    class Connection : Entity
    {
        public string UniqueName { get; set; }
    }

    class Program
    {
        

        static void Main(string[] args)
        {
            MongoClientFactory.ResetRegisteredClients();
            var repo = MongoRepositoryFactory.Create<Connection>("mongodb://localhost/FormSchema");
            var url = new MongoUrl("mongodb://localhost:27017/FormSchema");

            Console.WriteLine(url.DatabaseName);

            var registeredClients = MongoClientFactory.RegisteredClients;


            var connections = repo.Find(x => true);

            Console.WriteLine(connections.Count);
            Console.ReadLine();
        }
    }
}
